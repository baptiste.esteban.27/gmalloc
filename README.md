# GMALLOC

GMalloc is a very small library that avoid memory leaks in a C program.

Currently, there is just the function `gmalloc` which works like `malloc`

At the end of the project, there will be the functions :

* `gmalloc`: Like the `malloc` function
* `gfree`: Like the `free` function
* `gcalloc`: Like the `calloc` function
* `grealloc`: Like the `realloc` function

At the end of the project, the README will be rewritten.

For any errors or bugs, please send an email at baptiste.esteban.27@gmail.com
with the tag [GMALLOC]
