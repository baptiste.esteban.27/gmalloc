#include <stdlib.h>
#include <stdio.h>

#include "gmalloc.h"

void gmalloc_free();

struct gmalloc_list *get_gmalloc_list()
{
  static struct gmalloc_list *res = NULL;
  if (!res)
  {
    res = malloc(sizeof (struct gmalloc_list));
    if (!res)
      return NULL;
    res->head = NULL;
    int e = atexit(gmalloc_free);
    if (e != 0)
      exit(1);
  }
  return res;
}

int gmalloc_add(void *addr)
{
  struct gmalloc_list *l = get_gmalloc_list();
  struct gmalloc_node *n = malloc(sizeof (struct gmalloc_node));
  if (!n)
    return 1;
  n->addr = addr;
  n->next = l->head;
  l->head = n;
  return 0;
}

int gmalloc_rem(void *addr)
{
  struct gmalloc_list *l = get_gmalloc_list();
  struct gmalloc_node *tmp = l->head;
  struct gmalloc_node *old = tmp;
  while (tmp != NULL && tmp->addr != addr)
  {
    old = tmp;
    tmp = tmp->next;
  }
  if (!tmp)
    return 1;

  if (tmp == l->head)
    l->head = tmp->next;
  else
    old->next = tmp->next;
  free(addr);
  free(tmp);
  return 0;
}

void gmalloc_free_rec(struct gmalloc_node *n)
{
  if (n->next)
    gmalloc_free_rec(n->next);
  free(n->addr);
  free(n);
}

void gmalloc_free()
{
  struct gmalloc_list *l = get_gmalloc_list();
  if (l->head)
    gmalloc_free_rec(l->head);
  free(l);
}

void *gmalloc(size_t size)
{
  void *res = malloc(size);
  if (!res)
    return NULL;
  int e = gmalloc_add(res);
  if (e != 0)
  {
    free(res);
    return NULL;
  }
  return res;
}

void *gcalloc(size_t nmemb, size_t size)
{
  void *res = calloc(nmemb, size);
  if (!res)
    return NULL;
  int e = gmalloc_add(res);
  if (e != 0)
  {
    free(res);
    return NULL;
  }
  return res;
}

void gfree(void *addr)
{
  gmalloc_rem(addr);
}
