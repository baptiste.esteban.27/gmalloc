#ifndef GMALLOC_H
# define GMALLOC_H

# include <stddef.h>

struct gmalloc_list
{
  struct gmalloc_node *head;
};

struct gmalloc_node
{
  void *addr;
  struct gmalloc_node *next;
};

void *gmalloc(size_t size);
void *gcalloc(size_t nmemb, size_t size);
void gfree(void* addr);

#endif /* !GMALLOC_H */
